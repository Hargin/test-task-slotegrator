<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "presents".
 *
 * @property int $id
 * @property string $name
 * @property float $cost
 * @property int|null $count
 *
 * @property UserPresent[] $userPresents
 */
class Presents extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'presents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'cost'], 'required'],
            [['cost'], 'number'],
            [['count'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'cost' => 'Cost',
            'count' => 'Count',
        ];
    }

    /**
     * Gets query for [[UserPresents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserPresents()
    {
        return $this->hasMany(UserPresent::className(), ['present_id' => 'id']);
    }
}
