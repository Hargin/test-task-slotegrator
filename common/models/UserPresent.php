<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_present".
 *
 * @property int $user_id
 * @property int $present_id
 * @property int $count
 *
 * @property Presents $present
 * @property User $user
 */
class UserPresent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_present';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'present_id','count'], 'required'],
            [['user_id', 'present_id','count'], 'integer'],
            [['present_id'], 'exist', 'skipOnError' => true, 'targetClass' => Presents::className(), 'targetAttribute' => ['present_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'present_id' => 'Present ID',
        ];
    }

    /**
     * Gets query for [[Present]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPresent()
    {
        return $this->hasOne(Presents::className(), ['id' => 'present_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
