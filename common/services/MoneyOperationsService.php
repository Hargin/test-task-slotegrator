<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/15/2021
 * Time: 12:50 PM
 */

namespace common\services;

use common\models\MoneyPrices;
use common\models\Settings;
use common\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use linslin\yii2\curl\Curl;

class MoneyOperationsService
{
    /**
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public static function exchangeMoney(){
        $exchange = (float)Yii::$app->request->post('exchange');
        /** @var MoneyPrices[] $winningMoney */
        $winningMoney = MoneyPrices::find()->where(['user_id'=>Yii::$app->user->id])
            ->andWhere('sent=0')
            ->all();
        if($winningMoney&&$exchange<=array_sum(ArrayHelper::map($winningMoney,'id','amount'))){
            self::minusFromMoneyPrices($winningMoney,$exchange);
            $user = User::findOne(Yii::$app->user->id);
            $user->bonus_amount += $exchange*((float)Settings::findOne(2)->value);
            $user->save();
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param $n
     * @throws \Exception
     */
    public static function sendMoney($n){
        /** @var MoneyPrices[] $moneyPrices */
        $moneyPrices = MoneyPrices::find()->where('sent=0')->all();
        if($moneyPrices){
            for ($i=0;$i<$n;$i++){
                if(self::sendRequest($moneyPrices[$i]->amount)){
                    $moneyPrices[$i]->sent = 1;
                    $moneyPrices[$i]->save();
                }
            }
        }
    }

    /**
     * @param $win
     * @return bool
     * @throws \Exception
     */
    private static function sendRequest($win){
        $curl = new Curl();

        $curl->setOption(
            CURLOPT_POSTFIELDS,
            http_build_query(array(
                    'amount' => $win
                )
            ))
            ->post('http://example.com/');
        if($curl->responseCode==200){
            return true;
        }
        return false;
    }

    /**
     * @param MoneyPrices[] $winningMoney
     * @param $exchange
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    private static function minusFromMoneyPrices($winningMoney,$exchange){
        foreach ($winningMoney as $money){
            if($money->amount<=$exchange){
                $exchange -=$money->amount;
                $money->delete();
            }else{
                $money->amount -= $exchange;
                $money->save();
                break;
            }
            if($exchange==0){
                break;
            }
        }
    }
}