<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08.10.2018
 * Time: 8:28
 */

namespace console\controllers;

use common\services\MoneyOperationsService;
use yii\console\Controller;

class SettingsController extends Controller
{
    public $n;

    public function options($actionID)
    {
        return ['n'];
    }

    /**
     * @throws \Exception
     */
    public function actionSendPrices(){
        MoneyOperationsService::sendMoney($this->n);
    }
}