<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%presents}}`.
 */
class m210414_164109_create_presents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%presents}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'cost' => $this->float()->notNull(),
            'count' => $this->integer()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%presents}}');
    }
}
