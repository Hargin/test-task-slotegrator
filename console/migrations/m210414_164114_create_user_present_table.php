<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_product}}`.
 */
class m210414_164114_create_user_present_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_present}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'present_id' => $this->integer()->notNull(),
            'count' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-user_present-user_id',
            'user_present',
            'user_id'
        );

        $this->addForeignKey(
            'fk-user_present-user_id',
            'user_present',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            'idx-user_present-present_id',
            'user_present',
            'present_id'
        );

        $this->addForeignKey(
            'fk-user_present-present_id',
            'user_present',
            'present_id',
            'presents',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_product}}');
    }
}
