<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%settings}}`.
 */
class m210414_170256_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'value' => $this->string()->defaultValue(null),
        ]);

        $this->insert('settings',[
            'id' => 1,
            'name' => 'win amount left',
            'value' => '100'
        ]);
        $this->insert('settings',[
            'id' => 2,
            'name' => 'course money to bonuses',
            'value' => '1.3'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}
