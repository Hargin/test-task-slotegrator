<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%money_prices}}`.
 */
class m210415_091048_create_money_prices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%money_prices}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'amount' => $this->float()->notNull(),
            'sent' => $this->integer()->defaultValue(0),
        ]);

        $this->createIndex(
            'idx-money_prices-user_id',
            'money_prices',
            'user_id'
        );

        $this->addForeignKey(
            'fk-money_prices-user_id',
            'money_prices',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%money_prices}}');
    }
}
