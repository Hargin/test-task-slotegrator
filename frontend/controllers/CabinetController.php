<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/14/2021
 * Time: 8:44 PM
 */

namespace frontend\controllers;

use common\models\Settings;
use common\models\User;
use common\services\MoneyOperationsService;
use frontend\services\OpenBoxService;
use Yii;
use yii\web\Controller;

class CabinetController extends Controller
{
    /**
     * @param $action
     * @return bool|\yii\web\Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('site/login');
        }
        return parent::beforeAction($action);
    }

    public function actionIndex(){

        if(Yii::$app->session->get('win')){
            Yii::$app->session->setFlash('success', Yii::$app->session->get('win'));
            Yii::$app->session->remove('win');
        }

        return $this->render('index',[
           'user'=>User::find()->with(['moneyPrices','userPresent'])->where(['id'=>Yii::$app->user->id])->one(),
           'course'=>Settings::findOne(2)->value,
            'error'=>$_GET['error']??null
        ]);
    }

    /**
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionOpen(){
        $win = OpenBoxService::open();

        Yii::$app->session->set('win',$win);
        return $this->redirect('/cabinet?win='.$win);
    }

    /**
     * @return \yii\web\Response
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionExchange(){

        if(MoneyOperationsService::exchangeMoney()){

            Yii::$app->session->setFlash('success', "Бонусы начислены!");
            return $this->redirect('/cabinet');
        }else{
            return $this->redirect('/cabinet?error='.urlencode('Недостаточно денег на счету'));
        }
    }
}