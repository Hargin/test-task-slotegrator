<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 4/14/2021
 * Time: 9:34 PM
 */

namespace frontend\services;

use common\models\MoneyPrices;
use common\models\User;
use common\models\UserPresent;
use Yii;
use common\models\Presents;
use common\models\Settings;

class OpenBoxService
{
    /**
     * @return string
     * @throws \Exception
     */
    public static function open(){
        $amountCount = Settings::findOne(1);
        $presents = Presents::find()->where('count>0')->all();
        $curType = self::getType($amountCount,$presents);

        switch ($curType){
            case 'bonus': $win = self::setBonuses();break;
            case 'money': $win = self::setWinAmount($amountCount);break;
            case 'present': $win = self::setPresent($presents);break;
        }
        return $win;
    }

    private static function getType($amountCount,$presents){
        $arr = ['bonus'];
        if((float)$amountCount->value>0){
            $arr[] = 'money';
        }
        if($presents){
            $arr[] = 'present';
        }
        return $arr[mt_rand(0,count($arr)-1)];
    }

    private static function setBonuses(){
        $win = mt_rand(1,20);
        $user = User::findOne(Yii::$app->user->id);
        $user->bonus_amount += $win;
        $user->save();
        return 'Вы выиграли '.$win.' бонусов';
    }

    /**
     * @param Settings $amountCount
     * @return string
     * @throws \Exception
     */
    private static function setWinAmount($amountCount){
        $win = mt_rand(1,10);
        $winningMoney = new MoneyPrices();
        $winningMoney->user_id = Yii::$app->user->id;
        $winningMoney->amount = $win;
        $winningMoney->save();
        $amountCount->value = ((float)$amountCount->value-1).'';
        $amountCount->save();
        return 'Вы выиграли '.$win.' денег';
    }

    /**
     * @param Presents[] $presents
     * @return string
     */
    private static function setPresent($presents){
        $presentWin = mt_rand(0,count($presents)-1);
        /** @var UserPresent $userPresent */
        $userPresent = UserPresent::find()->where(['user_id'=>Yii::$app->user->id])
            ->andWhere(['present_id'=>$presents[$presentWin]->id])
            ->one();
        if(!$userPresent){
            $userPresent = new UserPresent();
            $userPresent->user_id = Yii::$app->user->id;
            $userPresent->present_id = $presents[$presentWin]->id;
            $userPresent->count = 1;
        }else{
            $userPresent->count += 1;
        }
        $userPresent->save();
        $presents[$presentWin]->count -=1;
        $presents[$presentWin]->save();
        return 'Вы выиграли '.$presents[$presentWin]->name.' подарок!';
    }

}