<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $user \common\models\User */
/* @var $course float */
/* @var $error string */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Кабинет пользователя';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Откройте подарок!</h1>

        <?=Html::a('Открыть!','/cabinet/open',['class'=>'btn btn-success'])?>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <p><b>Ваши данные: </b></p>
                <p><?=$user->username?></p>
                <p><?=$user->email?></p>
                <p>Выигрышные деньги: </p>
                <?php
                if($user->moneyPrices){
                foreach ($user->moneyPrices as $winningMoney){
                ?>
                    <p>Сумма: <?=$winningMoney->amount?> Переведено на счет: <?=$winningMoney->sent?'да':'нет'?></p>
                    <?php
                }
                }else{
                    echo '<p>Выигранных денег пока нет</p>';
                }?>
            </div>
            <div class="col-lg-4">
                <p><b>Бонусы: <?=$user->bonus_amount?></b></p>
                <p>Вы можете обменять ваши деньги на бонусы: </p>
                <p>Курс: 1 к <?=$course?></p>
                <?php $form = ActiveForm::begin(['method'=>'post','action'=>'/cabinet/exchange']); ?>
                <?=Html::textInput('exchange','',['class'=>'form-control','placeholder'=>'Введите сумму'])?>
                <p style="color:red;display: <?=$error?'block':'none'?>"><b><?=$error?></b></p>
                <?=Html::button('Обменять',['class'=>'btn btn-primary','type'=>'submit'])?>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-lg-4">
                <p><b>Ваши подарки: </b></p>
                <?php
                if($user->userPresent){
                    foreach ($user->userPresent as $present){
                        ?>
                <div class="row">
                    <div class="col-sm-6"><?=$present->present->name?></div>
                    <div class="col-sm-4">Количество: <?=$present->count?></div>
                </div>
                <?php
                    }
                }else{
                    echo '<p>Подарков пока нет</p>';
                }?>
            </div>
        </div>

    </div>
</div>
